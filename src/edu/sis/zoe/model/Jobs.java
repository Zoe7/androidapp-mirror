package edu.sis.zoe.model;

public class Jobs {

	int id;
	String title;
	int com_id;
	public Jobs(){
		
	}
	public Jobs(int id, String title, int comId) {
		super();
		this.id = id;
		this.title = title;
		this.com_id = comId;
	}
	public Jobs(String title, int comId) {
		super();
		this.title = title;
		this.com_id = comId;
	}
	public int getCom_id() {
		return com_id;
	}
	public void setCom_id(int com_id) {
		this.com_id = com_id;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
}
