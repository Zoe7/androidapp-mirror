package edu.sis.zoe.model;

public class Company {
	int id;
	String name;
	int add_id;
	public Company(){
		
	}
	public Company(int id, String name, int addId) {
		this.id = id;
		this.name = name;
		this.add_id = addId;
	}
	public Company(String name, int addId) {
		this.name = name;
		this.add_id = addId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAdd_id() {
		return add_id;
	}
	public void setAdd_id(int add_id) {
		this.add_id = add_id;
	}
	
}
