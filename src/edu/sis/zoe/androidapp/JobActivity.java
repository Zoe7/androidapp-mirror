package edu.sis.zoe.androidapp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.TextView;
import android.widget.Toast;
import edu.sis.zoe.model.Address;
import edu.sis.zoe.model.Company;
import edu.sis.zoe.model.Jobs;

public class JobActivity extends Activity {

	public static ExpandableListView expListView;
	public static ExpandableListAdapter listAdapter;
	public static List<String> listDataHeader;
	public static HashMap<String, List<String>> listDataChild;
	public static String text;
	DatabaseHandler db = new DatabaseHandler(this);
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_job);
		// Show the Up button in the action bar.
		listDataHeader = new ArrayList<String>();
		listDataChild = new HashMap<String, List<String>>();
		List<String> job_titles = db.getDistinctJobs();
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, job_titles);
		final AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView)findViewById(R.id.autoCompleteTextView1);
		autoCompleteTextView.setThreshold(1);
		autoCompleteTextView.setAdapter(adapter);
		Button b = (Button)findViewById(R.id.button1);
		
		b.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				listDataHeader.clear();
				listDataChild.clear();
				text = autoCompleteTextView.getText().toString();
				List<Jobs> jobs = db.getJobsByTitle(text);
				int count = jobs.size();
				TextView view = (TextView)findViewById(R.id.textView1);
				String content = "Find " + count + " jobs in total.";
				view.setText(content);
				expListView = (ExpandableListView)findViewById(R.id.expandableListView1);
				prepareData(text);
				listAdapter = new ExpandableListAdapter(JobActivity.this, listDataHeader, listDataChild);
				expListView.setAdapter(listAdapter);
				expListView.setOnGroupClickListener(new OnGroupClickListener(){

					@Override
					public boolean onGroupClick(ExpandableListView parent,
							View v, int groupPosition, long id) {
						// TODO Auto-generated method stub
						return false;
					}
				});
				
				expListView.setOnChildClickListener(new OnChildClickListener(){

					@Override
					public boolean onChildClick(ExpandableListView parent,
							View v, int groupPosition, int childPosition, long id) {
						// TODO Auto-generated method stub
						Toast.makeText(getApplicationContext(), listDataHeader.get(groupPosition)
								+ " : " + listDataChild.get(listDataHeader.get(groupPosition)).get(childPosition),
								Toast.LENGTH_SHORT).show();
						return false;
					}
					
				});
				
				expListView.setOnGroupExpandListener(new OnGroupExpandListener(){

					@Override
					public void onGroupExpand(int groupPosition) {
						// TODO Auto-generated method stub
						Toast.makeText(getApplicationContext(), 
								listDataHeader.get(groupPosition) + " Expanded", Toast.LENGTH_SHORT);
					}
				});
				
				expListView.setOnGroupCollapseListener(new OnGroupCollapseListener(){

					@Override
					public void onGroupCollapse(int groupPosition) {
						// TODO Auto-generated method stub
						Toast.makeText(getApplicationContext(), 
								listDataHeader.get(groupPosition) + " Collapsed", Toast.LENGTH_SHORT);
					}
				});
			}
		});
		setupActionBar();
	}
	
	public void prepareData(String text){
		List<Jobs> jobs = db.getJobsByTitle(text);
		
		for(int i = 0; i<jobs.size(); i++){
			String title = jobs.get(i).getTitle();
			listDataHeader.add(title);
			List<String> childInfo = new ArrayList<String>();
			int com_id = jobs.get(i).getCom_id();
			Company company = db.getCompany((long)com_id);
			String com_name = company.getName();
			String com_info = "Company: " + com_name;
			childInfo.add(com_info);
			int add_id = company.getAdd_id();
			Address add = db.getAddress((long)add_id);
			String street1 = add.getStreetLine1();
			String city = add.getCity();
			String state = add.getState();
			String zipcode = add.getZipcode();
			String add_info = "Address: " + street1 + ", " + city + ", " + state + " " + zipcode;
			childInfo.add(add_info);
			listDataChild.put(title, childInfo);
		}
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.job, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
