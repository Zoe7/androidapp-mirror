package edu.sis.zoe.androidapp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.annotation.TargetApi;
import android.app.Activity;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import edu.sis.zoe.model.Address;
import edu.sis.zoe.model.Company;

public class LocationActivity extends Activity {
	private GoogleMap googleMap;
	public static String selectedItem;
	DatabaseHandler db = new DatabaseHandler(this);

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_location);
		List<Company> companies = db.getAllCompany();
		ArrayList<String> names = new ArrayList<String>();
		for(Company com: companies){
			String name = com.getName();
			names.add(name);
		}
		Spinner dropdown = (Spinner)findViewById(R.id.spinner1);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, names);
		dropdown.setAdapter(adapter);
		dropdown.setOnItemSelectedListener(new OnItemSelectedListener(){

			@Override
			public void onItemSelected(AdapterView<?> parent, View v,
					int pos, long id) {
				// TODO Auto-generated method stub
				selectedItem = parent.getItemAtPosition(pos).toString();
				Company company = db.getCompanyByName(selectedItem);
				Address add = db.getAddress(company.getAdd_id());
				try{
					initilizeMap(add, selectedItem);
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
		// Show the Up button in the action bar.
		setupActionBar();
	}
	
	private void initilizeMap(Address add, String name){
		String address = add.getStreetLine1() + ", " + add.getCity() + ", " + add.getState();
		Geocoder geocoder = new Geocoder(this, Locale.US);
		try {
			List<android.location.Address> locations = geocoder.getFromLocationName(address, 5);
			if(locations.size()>0){
				android.location.Address loc = locations.get(0);
				if(googleMap == null){
					googleMap = ((MapFragment)getFragmentManager().findFragmentById(R.id.map)).getMap();
					final LatLng pointer = new LatLng(loc.getLatitude(), loc.getLongitude());
					googleMap.addMarker(new MarkerOptions()
											.position(pointer)
											.title(name)
											.snippet(address));
				}
			}
			else{
				if(googleMap == null){
					googleMap = ((MapFragment)getFragmentManager().findFragmentById(R.id.map)).getMap();
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/**
	 * Set up the {@link android.app.ActionBar}, if the API is available.
	 */
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	private void setupActionBar() {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			getActionBar().setDisplayHomeAsUpEnabled(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.location, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
