package edu.sis.zoe.androidapp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.TextView;
import edu.sis.zoe.model.Address;
import edu.sis.zoe.model.Company;
import edu.sis.zoe.model.Jobs;

public class CompanyActivity extends Activity {

	public static ExpandableListView expListView;
	public static ExpandableListAdapter listAdapter;
	public static List<String> listDataHeader;
	public static HashMap<String, List<String>> listDataChild;
	public static String text;
	DatabaseHandler db = new DatabaseHandler(this);
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_company);
		listDataHeader = new ArrayList<String>();
		listDataChild = new HashMap<String, List<String>>();
		List<String> com_names = new ArrayList<String>();
		List<Company> companies = db.getAllCompany();
		for(int i = 0; i<companies.size(); i++){
			com_names.add(companies.get(i).getName());
		}
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, com_names);
		final AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView)findViewById(R.id.autoCompleteTextView1);
		autoCompleteTextView.setThreshold(1);
		autoCompleteTextView.setAdapter(adapter);
		Button b = (Button)findViewById(R.id.button1);
		
		b.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				listDataHeader.clear();
				listDataChild.clear();
				text = autoCompleteTextView.getText().toString();
				expListView = (ExpandableListView)findViewById(R.id.expandableListView1);
				prepareData(text);
				listAdapter = new ExpandableListAdapter(CompanyActivity.this, listDataHeader, listDataChild);
				expListView.setAdapter(listAdapter);
				
			}
			
		});
		
		// Show the Up button in the action bar.
		
		setupActionBar();
	}

	public void prepareData(String name){
		List<Jobs> jobs = db.getJobsByCompany(name);
		
		for(Jobs job: jobs){
			String title = job.getTitle();
			listDataHeader.add(title);
			List<String> childInfo = new ArrayList<String>();
			int com_id = job.getCom_id();
			Company company = db.getCompany((long)com_id);
			int add_id = company.getAdd_id();
			Address add = db.getAddress((long)add_id);
			String street1 = add.getStreetLine1();
			String city = add.getCity();
			String state = add.getState();
			String zipcode = add.getZipcode();
			String add_info = "Address: " + street1 + ", " + city + ", " + state + " " + zipcode;
			childInfo.add(add_info);
			listDataChild.put(title, childInfo);
		}
	}
	
	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.company, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
