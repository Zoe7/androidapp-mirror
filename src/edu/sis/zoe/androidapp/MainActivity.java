package edu.sis.zoe.androidapp;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import edu.sis.zoe.model.Address;
import edu.sis.zoe.model.Company;
import edu.sis.zoe.model.Jobs;

/**
 * @author Zoe
 *
 */
public class MainActivity extends Activity {

	public final static String FIND_JOBS = "edu.sis.zoe.findJobs";
	public final static String ALL_JOBS = "edu.sis.zoe.showAllJobs";
	public final static String SEE_MAP = "edu.sis.zoe.seeMap";
	public final static String SEARCH_COMP = "edu.sis.zoe.searchCompanies";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		DatabaseHandler db = new DatabaseHandler(this);
		if(db.getAllJobs().size()==0)
			prepareData();
		setContentView(R.layout.activity_main);
		db.close();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// TODO Auto-generated method stub
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	/**
	 * Start a new Activity
	 * @param view
	 */
	public void dispatchJobs(View view){
		Intent myIntent = new Intent(this, JobActivity.class);
		String str = "HAVE TROUBLE FINDING JOB?";
		myIntent.putExtra(FIND_JOBS, str);
		startActivity(myIntent);
	}
	
	public void dispatchAllJobs(View view){
		Intent myIntent = new Intent(this, AllJobsActivity.class);
		String str = "SHOW ALL JOBS";
		myIntent.putExtra(ALL_JOBS, str);
		startActivity(myIntent);
	}
	
	public void dispatchLocation(View view){
		Intent myIntent = new Intent(this, LocationActivity.class);
		String str = "Search By Location";
		myIntent.putExtra(SEE_MAP, str);
		startActivity(myIntent);
	}
	
	public void dispatchCompany(View view){
		Intent myIntent = new Intent(this, CompanyActivity.class);
		String str = "Search By Company";
		myIntent.putExtra(SEARCH_COMP, str);
		startActivity(myIntent);
	}
	
	public void prepareData(){
		DatabaseHandler db = new DatabaseHandler(this);
		Log.d("Insert: ", "Inserting ..");
		
		Address add1 = new Address("1600 Amphitheatre Parkway", null, "Mountain View", "CA", "94043");
		Address add2 = new Address("1601 Willow Road", null, "Menlo Park", "CA", "94025");
		Address add3 = new Address("333 Boren Ave N", null, "Seattle", "WA", "98109");
		Address add4 = new Address("140 New Montgomery St", null, "San Francisco", "CA", "94105");
		Address add5 = new Address("2029 Stierlin Ct", null, "Mountain View", "CA", "94043");
		
		long add_id1 = db.createAddress(add1);
		long add_id2 = db.createAddress(add2);
		long add_id3 = db.createAddress(add3);
		long add_id4 = db.createAddress(add4);
		long add_id5 = db.createAddress(add5);
		
		Company com1 = new Company("Google", (int)(long)add_id1);
		Company com2 = new Company("Facebook", (int)(long)add_id2);
		Company com3 = new Company("Amazon", (int)(long)add_id3);
		Company com4 = new Company("Yelp", (int)(long)add_id4);
		Company com5 = new Company("LinkedIn", (int)(long)add_id5);
		
		long com_id1 = db.createCompany(com1, add_id1);
		long com_id2 = db.createCompany(com2, add_id2);
		long com_id3 = db.createCompany(com3, add_id3);
		long com_id4 = db.createCompany(com4, add_id4);
		long com_id5 = db.createCompany(com5, add_id5);
		
		Jobs job1 = new Jobs("JAVA Developer", (int)(long)com_id4);
		Jobs job2 = new Jobs("Web Developer", (int)(long)com_id4);
		Jobs job3 = new Jobs("Software Engineering", (int)(long)com_id1);
		Jobs job4 = new Jobs("Software Engineering Intern", (int)(long)com_id1);
		Jobs job5 = new Jobs("UI Designer", (int)(long)com_id2);
		Jobs job6 = new Jobs("Software Engineering", (int)(long)com_id3);
		Jobs job7 = new Jobs("JAVA Developer", (int)(long)com_id5);
		Jobs job8 = new Jobs("Web Developer", (int)(long)com_id5);
		
		db.createJob(job1, com_id4);
		db.createJob(job2, com_id4);
		db.createJob(job3, com_id1);
		db.createJob(job4, com_id1);
		db.createJob(job5, com_id2);
		db.createJob(job6, com_id3);
		db.createJob(job7, com_id5);
		db.createJob(job8, com_id5);
		db.closeDB();
	}

}
