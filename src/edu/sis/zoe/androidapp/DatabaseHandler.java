package edu.sis.zoe.androidapp;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import edu.sis.zoe.model.Address;
import edu.sis.zoe.model.Company;
import edu.sis.zoe.model.Jobs;

public class DatabaseHandler extends SQLiteOpenHelper {
	private static final String LOG = "DatabaseHandler";
	// Database Version
	private static final int DATABASE_VERSION = 1;
	// Database Name
	private static final String DATABASE_NAME = "jobApp";
	// Table Names
	private static final String TABLE_ADDRESS = "address";
	private static final String TABLE_COMPANY = "company";
	private static final String TABLE_JOB = "job";
	//Common column names
	private static final String KEY_ID = "id";
	// ADDRESS Table - column names
	private static final String STREETLINE1 = "streetLine1";
	private static final String STREETLINE2 = "streetLine2";
	private static final String CITY = "city";
	private static final String STATE = "state";
	private static final String ZIPCODE = "zipcode";
	// COMAPNY Table - column names
	private static final String NAME = "name";
	private static final String ADD_ID = "add_id";
	// JOBS Table - column names
	private static final String TITLE = "title";
	private static final String COM_ID = "com_id";
	
	// Table Create Statements
	// Address table create statement
	private static final String  CREATE_TABLE_ADDRESS = "CREATE TABLE "
            + TABLE_ADDRESS + "(" + KEY_ID + " INTEGER PRIMARY KEY," + STREETLINE1
            + " TEXT," + STREETLINE2 + " TEXT," + CITY 
            + " TEXT," + STATE + " TEXT, " + ZIPCODE + " TEXT" + ")";	
	
	// Company table create statement
	private static final String CREATE_TABLE_COMPANY = "CREATE TABLE "
            + TABLE_COMPANY + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + NAME + " TEXT," + ADD_ID + " INTEGER" + ")";
	
	// Job table create statement
	private static final String CREATE_TABLE_JOB = "CREATE TABLE "
            + TABLE_JOB + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
            + TITLE + " TEXT," + COM_ID + " INTEGER" + ")";
	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL(CREATE_TABLE_ADDRESS);
		db.execSQL(CREATE_TABLE_COMPANY);
		db.execSQL(CREATE_TABLE_JOB);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ADDRESS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_COMPANY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_JOB);
        db.execSQL(CREATE_TABLE_ADDRESS);
		db.execSQL(CREATE_TABLE_COMPANY);
		db.execSQL(CREATE_TABLE_JOB);
	}
	
	// Creating address
	public long createAddress(Address address){
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(STREETLINE1, address.getStreetLine1());
		values.put(STREETLINE2, address.getStreetLine2());
		values.put(CITY, address.getCity());
		values.put(STATE, address.getState());
		values.put(ZIPCODE, address.getZipcode());
		long add_id = db.insert(TABLE_ADDRESS, null, values);
		return add_id;
	}
	// getting single address
	public Address getAddress(long add_id){
		SQLiteDatabase db = this.getReadableDatabase();
		String selectQuery = "SELECT * FROM " + TABLE_ADDRESS + " WHERE " + KEY_ID + " = " + add_id; 
		Log.e(LOG, selectQuery);
		Cursor c = db.rawQuery(selectQuery, null);
		if(c!=null){
			c.moveToFirst();
		}
		Address add = new Address();
		add.setId(c.getInt(c.getColumnIndex(KEY_ID)));
		add.setStreetLine1(c.getString(c.getColumnIndex(STREETLINE1)));
		add.setStreetLine2(c.getString(c.getColumnIndex(STREETLINE2)));
		add.setCity(c.getString(c.getColumnIndex(CITY)));
		add.setState(c.getString(c.getColumnIndex(STATE)));
		add.setZipcode(c.getString(c.getColumnIndex(ZIPCODE)));
		return add;
	}
	// getting all address
	public List<Address> getAllAddress(){
		List<Address> addresses = new ArrayList<Address>();
		String selectQuery = "SELECT * FROM " + TABLE_ADDRESS;
		Log.e(LOG, selectQuery);
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);
		if(c.moveToFirst()){
			do{
				Address add = new Address();
				add.setId(c.getInt(c.getColumnIndex(KEY_ID)));
				add.setStreetLine1(c.getString(c.getColumnIndex(STREETLINE1)));
				add.setStreetLine2(c.getString(c.getColumnIndex(STREETLINE2)));
				add.setCity(c.getString(c.getColumnIndex(CITY)));
				add.setState(c.getString(c.getColumnIndex(STATE)));
				add.setZipcode(c.getString(c.getColumnIndex(ZIPCODE)));
				addresses.add(add);
			}
			while(c.moveToNext());
		}
		return addresses;
	}
	
	// Creating Company
	public long createCompany(Company company, long add_id){
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(NAME, company.getName());
		values.put(ADD_ID, add_id);
		long id = db.insert(TABLE_COMPANY, null, values);
		return id;
	}
	// getting single company by id
	public Company getCompany(long com_id){
		String selectQuery = "SELECT * FROM " + TABLE_COMPANY + " WHERE " + KEY_ID + " = " + com_id;
		Log.e(LOG, selectQuery);
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);
		if(c!=null){
			c.moveToFirst();
		}
		Company company = new Company();
		company.setId(c.getInt(c.getColumnIndex(KEY_ID)));
		company.setName(c.getString(c.getColumnIndex(NAME)));
		company.setAdd_id(c.getInt(c.getColumnIndex(ADD_ID)));
		return company;
	}
	
	public Company getCompanyByName(String name){
		String selectQuery = "SELECT * FROM " + TABLE_COMPANY + " WHERE " + NAME + " = '" + name + "'";
		Log.e(LOG, selectQuery);
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);
		if(c!=null){
			c.moveToFirst();
		}
		Company company = new Company();
		company.setId(c.getInt(c.getColumnIndex(KEY_ID)));
		company.setName(c.getString(c.getColumnIndex(NAME)));
		company.setAdd_id(c.getInt(c.getColumnIndex(ADD_ID)));
		return company;
	}
	
	// get all companies
	public List<Company> getAllCompany(){
		List<Company> companies = new ArrayList<Company>();
		String selectQuery = "SELECT * FROM " + TABLE_COMPANY;
		Log.e(LOG, selectQuery);
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);
		if(c.moveToFirst()){
			do{
				Company com = new Company();
				com.setId(c.getInt(c.getColumnIndex(KEY_ID)));
				com.setName(c.getString(c.getColumnIndex(NAME)));
				com.setAdd_id(c.getInt(c.getColumnIndex(ADD_ID)));
				companies.add(com);
			}
			while(c.moveToNext());
		}
		return companies;
	}
	
	// Creating Jobs
	public long createJob(Jobs job, long com_id){
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(TITLE, job.getTitle());
		values.put(COM_ID, com_id);
		long id = db.insert(TABLE_JOB, null, values);
		return id;
	}
	
	// get all jobs
	public List<Jobs> getAllJobs(){
		List<Jobs> jobs = new ArrayList<Jobs>();
		String selectQuery = "SELECT * FROM " + TABLE_JOB;
		Log.e(LOG, selectQuery);
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);
		if(c.moveToFirst()){
			do{
				Jobs job = new Jobs();
				job.setId(c.getInt(c.getColumnIndex(KEY_ID)));
				job.setTitle(c.getString(c.getColumnIndex(TITLE)));
				job.setCom_id(c.getInt(c.getColumnIndex(COM_ID)));
				jobs.add(job);
			}
			while(c.moveToNext());
		}
		return jobs;
	}
	
	// get distinct jobs
	public List<String> getDistinctJobs(){
		List<String> job_title = new ArrayList<String>();
		String selectQuery = "SELECT DISTINCT " + TITLE + " FROM " + TABLE_JOB;
		Log.e(LOG, selectQuery);
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);
		if(c.moveToFirst()){
			do{
				job_title.add(c.getString(c.getColumnIndex(TITLE)));
			}
			while(c.moveToNext());
		}
		return job_title;
	}
	
	// get jobs based on a specific company
	public List<Jobs> getJobsByCompany(String com_name){
		List<Jobs> jobs = new ArrayList<Jobs>();
		String selectQuery = "SELECT * FROM " + TABLE_COMPANY + " c, "
				+ TABLE_JOB + " j WHERE c." + NAME + " = '" + com_name
				+ "' AND c." + KEY_ID + " = " + "j." + COM_ID;
		Log.e(LOG, selectQuery);
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);
		if(c.moveToFirst()){
			do{
				Jobs job = new Jobs();
				job.setId(c.getInt(c.getColumnIndex(KEY_ID)));
				job.setTitle(c.getString(c.getColumnIndex(TITLE)));
				job.setCom_id(c.getInt(c.getColumnIndex(COM_ID)));
				jobs.add(job);
			}
			while(c.moveToNext());
		}
		return jobs;
	}
	
	// get jobs based on title
	public List<Jobs> getJobsByTitle(String title){
		List<Jobs> jobs = new ArrayList<Jobs>();
		String selectQuery = "SELECT * FROM " + TABLE_JOB + " WHERE " + TITLE
				+ " LIKE '%" + title + "%'";
		Log.e(LOG, selectQuery);
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(selectQuery, null);
		if(c.moveToFirst()){
			do{
				Jobs job = new Jobs();
				job.setId(c.getInt(c.getColumnIndex(KEY_ID)));
				job.setTitle(c.getString(c.getColumnIndex(TITLE)));
				job.setCom_id(c.getInt(c.getColumnIndex(COM_ID)));
				jobs.add(job);
			}
			while(c.moveToNext());
		}
		return jobs;
	}
	
	public void closeDB(){
		SQLiteDatabase db = this.getReadableDatabase();
		if(db != null && db.isOpen())
			db.close();
	}
}
